#include"a.h"
#include<sys/mman.h>
#include<unistd.h>
#include<fcntl.h>
S D clPt(A p,A q,A p1,A q1,A*r)_(A u=sub(q,p),v=sub(q1,p1),w=sub(p,p1); //closest point
 D a=l2(u),b=dot(u,v),c=l2(v),d=dot(u,w),e=dot(v,w),f=a*c-b*b,sc,sN,sD=f,tc,tN,tD=f;
 Y(f<1e-8,sN=0;tN=e;tD=c)E(sN=b*e-c*d;tN=a*e-b*d;Y(sN<0,sN=0;tN=e;tD=c)EY(sN>sD,sN=sD;tN=e+b;tD=c))
 Y(tN<0,tN=0;Y(-d<0,sN=0)EY(-d>a,sN=sD)E(sN=-d;sD=a))EY(tN>tD,tN=tD;Y(b<d,sN=0)EY(d+a<b,sN=sD)E(sN=b-d;sD=a))
 sc=sN/sD;tc=tN/tD;A dP=add(add(w,mul(u,sc)),mul(v,-tc));*r=add(p,mul(u,sc));l2(dP))
S B d3(D a,D b,D c)_(a<=b&&b<=c)A GKP(B l,B t)_(A(OX+(l?-Y6W/2:Y6W/2),t?BY0+Y6H:BY1-Y6H))
B xSL(A p,A q,A*r)_(D x=q[0];P(d3(BX0,x,BX1),0)x=BX[x>BX0];clPt(z0(p),z0(q),A(x,BY0),A(x,BY1),r);1) //crosses side line
B xGL(A p,A q,A*r)_(D y=q[1];P(d3(BY0,y,BY1),0)y=BY[y>BY0];clPt(z0(p),z0(q),A(BX0,y),A(BX1,y),r);1) //crosses goal line
B  xG(A p,A q,A*r)_(xGL(p,q,r)&&d3(GX0,r[0][0],GX1)&&q[2]<GZ) //crosses goal
A clGKk(A p)_(GKP(p[0]<OX,p[1]<OY)) //closest goal kick spot
B inO(A p)_(ll(sub(p,OP),OR)) //in central circle
B inA(A p,B t)_(D x=p[0],y=p[1];d3(AX0,x,AX1)&&(t?d3(BY0,y,AY0):d3(AY1,y,BY1))) //in penalty area

S I p0(G g){I b=g->format->BytesPerPixel;UC*p=(UC*)g->pixels;I v; //first point
 SW(b){DF:v=0;BR;CA 1:v=*p;BR;CA 2:v=*(UH*)p;BR;CA 4:v=*(I*)p;BR;
  CA 3:v=SDL_BYTEORDER==SDL_BIG_ENDIAN?p[0]<<16|p[1]<<8|p[2]:p[0]|p[1]<<8|p[2]<<16;BR;}R v;}
S G ld(O C*s,B c,B a)_(G g=IMG_Load(s); //load
 Y(!g,SDL_RWops*o=SDL_RWFromFile(s,"r");assert(o);g=IMG_LoadTGA_RW(o);SDL_RWclose(o);assert(g))
 Y(c,SDL_SetColorKey(g,SDL_SRCCOLORKEY|SDL_RLEACCEL,p0(g)))
 Y(a,SDL_SetAlpha(g,SDL_SRCALPHA|SDL_RLEACCEL,SDL_ALPHA_OPAQUE))
 G r=c||a?SDL_DisplayFormatAlpha(g):SDL_DisplayFormat(g);SDL_FreeSurface(g);r)
G_;S G gL[TH][TW];S G gQ[2][2][5][8][40];S O I an[]={40,1,40,25,20};S O B ar[]={1,1,1,0,0};
S G bgr(G g){assert(g->format->BytesPerPixel==4);SDL_PixelFormat*f=g->format; //blue-green-red
 G r=SDL_CreateRGBSurface(g->flags,g->w,g->h,f->BitsPerPixel,f->Rmask,f->Gmask,f->Bmask,f->Amask);
 UC*a=(UC*)g->pixels,*b=(UC*)r->pixels;i(g->w*g->h,b[0]=a[2];b[1]=a[1];b[2]=a[0];b[3]=a[3];a+=4;b+=4)R r;}
S B w(C c)_(c==10||c==32)
#define h(x,c,a) g_##x=ld("g/" #x ".png",c,a);
S Q glQ[2];
V ig(){h(tile,0,0)h(tf,0,1)h(sc,0,1)h(bg,0,1)h(ht,0,1)h(ft,0,1)h(fy,0,1)h(fw,0,1)h(b,1,0)h(bs,1,0)h(m0,1,0)h(m1,1,0) //init graphics
 iQ(&bq,g_b,g_bs,0,OP,SR{-4,-9,0,0},SR{-1,-7,0,0},SR{});aQ(&bq);
 i(2,iQ(glQ+i,ld(i?"g/g1.tga":"g/g0.tga",1,0),0,0,A(OX,BY[i]),{-256,(H)(i?-110:-150),0,0},{},{});aQ(glQ+i))
 C s[16]="g/1./././..";F_(gk,2,s[3]='0'+gk;i(5,s[5]='0'+i;j(8,s[7]='0'+j;F_(k,an[i],s[9]='0'+k/10;s[10]='0'+k%10;
                        gQ[0][gk][i][j][k]=bgr(gQ[1][gk][i][j][k]=ld(s,1,0))))))
 I f=open("g/l/tile.map",0);off_t l=lseek(f,0,SEEK_END);
 O C*p=(O C*)mmap(0,l,PROT_READ,MAP_FILE|MAP_SHARED,f,0),*q=p;close(f);C b[8];strcpy(b,"g/l/...");
 i(TH,j(TW,W(w(*q),q++)O C*t=q++;W(!w(*q),q++)Y(*t-'.',b[4]=b[5]=b[6]='0';memcpy(b+7-(q-t),t,q-t);gL[i][j]=ld(b,0,1))))
 munmap((V*)p,l);}
#undef h

S Q*a[32];S I n;V aQ(Q*o){assert(n<ZZ(a));a[n++]=o;}S D y(O V*p)_((*(Q**)p)->_p[1])S I c(O V*p,O V*q)_(y(p)-y(q)) //Sprites
V iQ(Q*q,G g0,G g1,G g2,A p,SR o0,SR o1,SR o2){ //0:object,1:shadow,2:overlay
 q->_g[0]=g0;q->_g[1]=g1;q->_g[2]=g2;q->_p=p;q->_o[0]=o0;q->_o[1]=o1;q->_o[2]=o2;}
V iSq(Sq*x,B t,B gk,I a){x->_n=an[a];x->_r=ar[a];x->_i=0;x->_g=gQ[t][gk][a];}
V uQ(Sq*s){s->_i++;Y(s->_i>=s->_n,s->_i=s->_r?0:s->_i-1)}
V dr(I x,I y){G g=g_tile;SR r={0,0,(UH)g->w,(UH)g->h};I x1=(x/g->w)*g->w-x; //draw
 W(x1<g0->w,I y1=(y/g->h)*g->h-y;W(y1<g0->w,SR s;s.x=x1;s.y=y1;s.w=s.h=0;SB(g,&r,g0,&s);y1+=g->h)x1+=g->w)
 SR t={0,0,TS,TS},s=t;
 for(I i=max(0,y/TS),ey=min(TH,(y+g0->h)/TS+1);i<ey;i++)
  for(I j=max(0,x/TS),ex=min(TW,(x+g0->w)/TS+1);j<ex;j++){s.y=i*TS-y;s.x=j*TS-x;G g=gL[i][j];Y(g,SB(g,&t,g0,&s))}
 qsort(a,n,Z(*a),c);s.w=s.h=0;
 i(n,Q*q=a[i];G g=q->_g[1];Y(g,s.x=q->_p[0]-x+q->_o[1].x+q->_p[2];s.y=q->_p[1]-y+q->_o[1].y-q->_p[2]*.8;SB(g,0,g0,&s)))
 i(n,Q*q=a[i];G g=q->_g[0];Y(g,s.x=q->_p[0]-x+q->_o[0].x;s.y=q->_p[1]-y+q->_o[0].y-q->_p[2];SB(g,0,g0,&s)))
 i(n,Q*q=a[i];G g=q->_g[2];Y(g,s.x=q->_p[0]-x+q->_o[2].x;s.y=q->_p[1]-y+q->_o[2].y-q->_p[2];
  D x1=g0->w-g->w;s.x=s.x<0?0:s.x>=x1?x1:s.x;D y1=g0->h-g->h;s.y=s.y<0?0:s.y>=y1?y1:s.y;SB(g,0,g0,&s)))}
