#ifndef A_H
#define A_H
#include<assert.h>
#include<math.h>
#include<time.h>
#include<SDL/SDL_mixer.h>
#include<SDL/SDL_image.h>
#include"f.h"
#define      _(a...) {R({a;});}
#define    Y(x,a...) if(x){a;}
#define      E(a...) else{a;}
#define   EY(x,a...) else Y(x,a)
#define    P(x,a...) if(x)_(a)
#define    W(x,a...) while(x){a;}
#define F_(i,x,a...) for(I i=0,n_=(x);i<n_;i++){a;}
#define      i(a...) F_(i,a)
#define      j(a...) F_(j,a)
#define     O_(a...) O _(a)
#define O const
#define R return
#define S static
#define Z sizeof
#define ZZ(x) ((I)(Z(x)/Z(*(x))))
#define ST struct
#define VI virtual
#define OPE operator
#define TD typedef
#define XT extern
#define SW switch
#define CA case
#define DF default
#define BR break
#define SB SDL_BlitSurface
#define oO INFINITY
#define Oo (-oO)
#define min(x,y) ({I x_=(x),y_=(y);x_<y_?x_:y_;})
#define max(x,y) ({I x_=(x),y_=(y);x_>y_?x_:y_;})
#define in(i,n) ({I i_=(i);0<=i_&&i_<(n);})
#define MIX(x) Mix_PlayChannel(-1,x,0)
#define _T(x) {clock_t t0=clock();x;clock_t t1=clock();printf("%5lldms " #x "\n",(t1-t0)*1000ll/CLOCKS_PER_SEC);fflush(stdout);}
TD void V;TD bool B;TD char C;TD unsigned char UC;TD short H;TD unsigned short UH;TD int I;TD unsigned int UI;TD double D;
ST Pl;ST T;ST Mn;ST MK;ST MO;TD SDL_Surface*G;TD SDLKey K;TD SDL_Rect SR;
ST A{A(D=0,D=0,D=0);D _[3];D&OPE[](I);};ST M{M();D _[4][4];};
D dot(A,A),l2(A),l1(A);B ll(A,D),inArc(A,A,A);M roll(D),arb(A,D);A nrm(A),z0(A),add(A,A),sub(A,A),mul(A,D),mmu(M,A);

#define PW 2560
#define PH 4096
#define TS 16
#define TW (PW/TS)
#define TH (PH/TS)
S O D grv=.06,fri=.975,bnc=.6,kkH=32,OX=1280,OY=2048,OR=300, //gravity,friction,bounce factor,kick height,central circle
 BW=2160,BX0=OX-BW/2,BX1=OX+BW/2,BX[]={BX0,BX1}, //pitch bounds
 BH=3696,BY0=OY-BH/2,BY1=OY+BH/2,BY[]={BY0,BY1},
 GW=492 ,GX0=OX-GW/2,GX1=OX+GW/2,GX[]={GX0,GX1},GZ=100,GD=40,KY[]={BY0+32,BY1-32}, //goals and keepers
 AW=1052,AX0=OX-AW/2,AX1=OX+AW/2, //penalty areas
 AD=2696,AY0=OY-AD/2,AY1=OY+AD/2,
 Y6W=600,Y6H=150; //6-yard areas
S O A OP(OX,OY); //central spot
S O D CD=16;S O A CP[][2]={{{BX0+CD,BY0+CD},{BX0+CD,BY1-CD}},{{BX1-CD,BY0+CD},{BX1-CD,BY1-CD}}}; //corners
ST Q{A _p;G _g[3];SR _o[3];};ST Bo{A _p,_v;};ST Sq{G(*_g)[40];I _n,_i;B _r;}; //Q:sprite,Bo:body,Sq:sequence
A clGKk(A),GKP(B,B),calcReqV(D,A);B xSL(A,A,A*),xGL(A,A,A*),xG(A,A,A*),inA(A,B),inO(A);
V ig(),aQ(Q*),dr(I,I),apKkMvBa(),kkBa(O A&,I,Pl*),uCa(),uQ(Sq*),iSq(Sq*,B,B,I),iQ(Q*,G,G,G,A,SR,SR,SR);

ST MI{MI(Mn*,O C*);VI O C*text()O=0;VI I val()O=0,w(SF*)=0;O C*txtO(O C*)O;O C*_s;Mn*_m;
 VI V kl(){};VI V kr(){};VI V ks(){};VI V upd(UC*){};VI V dr(SF*,I,I,I,G);};
ST Mn{Mn();VI~Mn();I _n,_i,_px,_py;B _fin,_selMd,_show;SR _r;MI*_foc,*_a[32];
 V add(MI*),show(),dr(),upd(UC*),calcB(),sFi(),foc(MI*);};
ST MMn:Mn{MMn();~MMn();MO*_hl,*_pls;};
ST KMn:Mn{KMn();~KMn();MK*_k[2][6];};
ST VMn:Mn{VMn(B);~VMn();B curMd()O;B _fs;MO*_mo;};

enum Mv{Sta,Lmp,Wlk,Run,Tkl,Hvy,Pas,Sht,Hdr,Fal}; //moves: stand,limp,walk,run,tackle,heavy-tackle,pass,shoot,header,fall
enum CS{Full,K1,Nn}; //control states: full,kick-only,none
enum SM{Pla,Cor,GlK,Kk0,Pen,FrK,Thr,Cel}; //submodes: play,corner,goal-kick,kick-off,penalty,free-kick,throw-in,celebration
enum GS{MS=0,KS,VS,M0,An,H1,HT,H2,FT,X0,X1,X2,Fi}; //game states: main screen,keyboard screen,video mode screen,..
// ..match start,anthem,first half,half time,second half,full tie,extra time,extra first half,extra second half,final
ST Pl:Bo{Mv _m;CS _cS;B _jt,_ac,_gk;Q _q;Sq _sq[5];B _t;I _dir,_dirCnt,_cmitT,_kkCntdn,_tcTmr,_drblTmr;A _d;};
ST T{I _i,_d,_m,_a;Pl _t[11];B _h;CS _cS;Pl*_acPl,*_pl0,*_pl1;};
ST Co{I _shtCnt,_laPas;B _canKk;Pl*_laPl;I _i;K _k[6];};
V iT(T*,I,B),uT(T*),uPl(Pl*),anth(T*),kk0(T*,B),glK(T*,B,B),thr(T*,A,B),cor(T*,B,B),ht(T*),uCo(Co*,UC*),sDir(Pl*,I),
 sCS(T*,CS),sPlCS(Pl*,CS),iCo(Co*,I,O K*,B),iPl(Pl*,B,B);
Pl*cl1(T*,A),*plFmP(T*,A,I);B tcngBa(Pl*,B),rdy(T*),aft(Pl*,I),inP(Pl*),cmtd(Pl*),sMv(Pl*,Mv,I=0);I clDir(Pl*,O A&);

#define G_ G g_tf,g_tile,g_sc,g_bg,g_ht,g_ft,g_b,g_bs,g_fy,g_fw,g_m0,g_m1;
XT G_;XT G g0;XT A DIR[];XT B tS,gmFi,gmFS;XT Co co[2];XT B coAI[2];
XT Pl*laTc;XT SF*fY,*fW;XT T tm[2];XT UI hl,sc[2],tmr;
XT A*ct,cp,cv;XT SR cr; //camera: target,position,velocity,rectangle
XT A blp,bp,bv;XT Q bq; //ball: last position,position,velocity,sprite

#define SND h(beep)h(bnc)h(boo)h(cheer)h(ooh)h(dec)h(inc)h(kk)h(0)h(1)h(2)h(3)h(sel)
#define h(x) XT Mix_Chunk*s_##x;
SND;
#undef h

#endif
