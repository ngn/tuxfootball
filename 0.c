#include"a.h"
#define h(x) Mix_Chunk*s_##x;
 SND
#undef h
#define t(a...) i(2,T*t=tm+i;a)
S O C*tms[]={"Blue Utd","Red City"},*tm3[]={"BLU","RED"};
S Mn*mn;S I RW=1024,RH=600;S Mix_Music*mus;S A baPrP;S B baPr,smh;S SM sm;S GS gS;
G g0;A bp,bv;Pl*laTc;UI hl=6000,sc[2];Co co[2];T tm[2];SF*fY,*fW;UI tmr;B tS,gmFi,gmFS=1; S I sS(GS);S V sSM(SM,B,B,A);
S V tZ(){tS=0;}S V tR(){tmr=0;tS=1;}S V x2(){i(2,tm[i]._h^=1)}S V ht2(){i(2,ht(tm+i))}S B rd2()_(i(2,P(!rdy(tm+i),0))1)
S V sFS(B x){Y(gmFS^x,gmFS=x;SDL_WM_ToggleFullScreen(g0))}S V sMn(Mn*m){Y(mn,delete mn)mn=m;}S V prBa(A p){baPr=1;baPrP=p;}
S V pause(UI t,GS s){Y(!tS,Y(rd2(),tR()))EY(tmr>t,sS(s);tZ())}S B gmOn(I x)_(x==H1||x==H2||x==X1||x==X2)
S V sMus(O C*s){Y(mus,Mix_FreeMusic(mus);mus=0)mus=Mix_LoadMUS(s);Y(mus,Mix_PlayMusic(mus,-1))}
S V sSM(SM x,B h,B l=0,A p=A()){ //x:submode,h:home,l:left,p:pos
 SW(sm=x){CA Pen:CA FrK:BR;CA Cel:                   MIX(s_2);BR;
  CA Pla:t(sCS(t,Full ))tS=1;                                 BR;
  CA Kk0:t(kk0(t,i^h  ))tS=0;prBa(OP);                        BR;
  CA Cor:t(cor(t,l,i^h))tS=0;prBa(CP[!l][!tm[h]._h]);MIX(s_2);BR;
  CA Thr:t(thr(t,p,i^h))tS=0;prBa(p);                MIX(s_2);BR;
  CA GlK:t(glK(t,i^h,l))tS=0;prBa(GKP(l,tm[!h]._h)); MIX(s_2);BR;}
 smh=h;}
S V mtc0(){bp=OP;bv=0;baPr=0;sSM(Pla,0);tR();tZ();sc[0]=sc[1]=0;ct=&bp;}
S I rS(I x)_(P(x==MS||x==KS||x==VS,Y(mn,mn->dr())x) //render state
 P(x==HT||x==FT,G g=x==HT?g_ht:g_ft;SR s;s.x=(g0->w-g->w)/2;s.y=50;s.w=g->w;s.h=g->h;SB(g,0,g0,&s);x)
 P(x==H1||x==H2||x==X1||x==X2,UH a=g_sc->w;SR p={(H)(g0->w-a-10),10,a,28};SB(g_sc,0,g0,&p);C s[2][8];
  i(2,sprintf(s[i],"%d",sc[i]);SFw(g0,fW,g0->w-228+34*i,12,s[i]);SFw(g0,fY,g0->w-160-135*i,12,tm3[i]))
  SFw(g0,fW,g0->w-207,12,"-");I w=min(SFtw(fW,s[0]),SFtw(fW,s[1]))+20,m=g0->w/2,y=g0->h-34;SFw(g0,fW,m-w,y,s[0]);
  SFw(g0,fW,m+20,y,s[1]);SFw(g0,fY,m-w-SFtw(fY,tms[0])-20,y,tms[0]);SFw(g0,fY,m+w+20,y,tms[1]);
  I t=tmr*2700/hl;C z[8];sprintf(z,"%d:%02d",t/60,t%60);SFw(g0,fW,g0->w-SFtw(fW,z)-22,12,z);x)x)
S I sS(GS x)_(gS=x;P(x==H1,tR();sSM(Kk0,1);x)P(x==H2,x2();sSM(Kk0,0);tR();tZ();x)P(x==X0,sS(X1);x) //set state
 P(x==X1||x==X2,x2();sSM(Kk0,0);tR();tZ();x)P(x==HT,ht2();x)P(x==FT,Y(sc[0]==sc[1],sS(X0);tZ())E(ht2());x)P(x==Fi,sS(MS);x)
 P(x==MS,Mn*m=new MMn();m->show();sMn(m);ct=0;ht2();sMus("s/mus.ogg");x)
 P(x==KS,Mn*m=new KMn();m->show();sMn(m);ht2();x)P(x==VS,Mn*m=new VMn(gmFS);m->show();sMn(m);x)
 P(x==M0,mtc0();sMus(0);sS(An);tZ();x)P(x==An,i(2,anth(tm+i));x)x)
S I uS(GS x)_(P(x==An,pause(100,H1);x)P(x==HT,pause(300,H2);x)P(x==FT,pause(300,Fi);x) //update state
 P(x==H1||x==H2,Y(tmr>hl,sS(x==H1?HT:FT);MIX(x==H1?s_2:s_3);tZ())x)P(x==X1||x==X2,Y(tmr>hl/3,sS(x==X1?X2:Fi);MIX(s_3);tZ())x)
 P(x==MS,Y(!mn,gmFi=1)EY(mn->_selMd,I i=mn->_i;Y(!i,sS(M0))EY(i==3,sS(VS))EY(i==4,sS(KS))E(gmFi=1))Y(mn&&mn->_fin,sMn(0))x)
 P(x==KS,Y(mn,Y(mn->_selMd,sS(MS)))E(sS(MS))x)P(x==VS,P(mn&&!mn->_selMd,0)Y(mn,sFS(((VMn*)mn)->curMd()))sS(MS);x)x)
S V eng(){uS(gS);t(uT(t))SDL_Event e;W(SDL_PollEvent(&e),gmFi|=e.type==SDL_QUIT)
 UC*k=SDL_GetKeyState(0);i(2,uCo(co+i,k))S I kf;Y(k[SDLK_f]<kf,sFS(!gmFS))kf=k[SDLK_f];
 Y(gS==An&&k[SDLK_RETURN],sS(H1);tZ())Y((gS==MS||gS==KS||gS==VS)&&mn,mn->upd(k))
 S B qt;Y(k[SDLK_ESCAPE],MS?gmFi=1:(qt=1))EY(qt,sS(MS);qt=0)apKkMvBa();
 Y(tm[0]._cS==K1&&!ll(bv,.1),sSM(Pla,0))Y(baPr&&l2(bv)<.01,bp=baPrP;bv=0;baPr=0)uCa();
 Y(gmOn(gS),SW(sm){CA Pen:CA FrK:sSM(Pla,0);BR;CA Cel:sSM(Kk0,!smh);BR;
  CA Cor:CA GlK:CA Kk0:CA Thr:Y(rdy(tm)&&rdy(tm+1)&&!baPr&&(tm[0]._cS!=K1||tm[1]._cS!=K1),t(sCS(t,K1))MIX(s_0))BR;
  CA Pla:{A p=blp,q=bp,r;
   Y(xSL(p,q,&r),prBa(r);sSM(Thr,laTc&&laTc->_t,0,r))
   EY(xGL(p,q,&r),Y(xG(p,q,&r),I t=(r[1]<OY)==tm[0]._h;sc[t]++;MIX(t?s_boo:s_cheer);sSM(Cel,!t))
                  EY((r[1]<OY)==tm[laTc->_t]._h,sSM(Cor,laTc&&laTc->_t,r[0]<OX))
                  E(MIX(s_ooh);prBa(clGKk(r));sSM(GlK,laTc&&laTc->_t,r[0]<OX)))BR;}})}
I main(){SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER);atexit(SDL_Quit);SDL_WM_SetCaption("tuxfootball",0);
 g0=SDL_SetVideoMode(RW,RH,32,SDL_SWSURFACE|SDL_FULLSCREEN*gmFS);ig();S UC c[8];SDL_SetCursor(SDL_CreateCursor(c,c,8,8,4,4));
 #define h(x) s_##x=Mix_LoadWAV("s/" #x ".wav");
  fY=SFi(g_fy);fW=SFi(g_fw);Mix_OpenAudio(44100,MIX_DEFAULT_FORMAT,2,1024);SND;
 #undef h
 O K k[][6]={{SDLK_UP,SDLK_DOWN,SDLK_LEFT,SDLK_RIGHT,SDLK_LSHIFT,SDLK_LCTRL},{SDLK_w,SDLK_s,SDLK_a,SDLK_d,SDLK_j,SDLK_k}};
 i(2,iT(tm+i,i,!i);iCo(co+i,i,k[i],i))cr={(H)(RW/2),(H)(RH/2),(UH)(PW-RW),(UH)(PH-RH)};
 mus=0;I t1=SDL_GetTicks();cp=A(OX,PH-g0->h);gmFi=0;sS(MS);mtc0();
 W(!gmFi,I t=SDL_GetTicks();Y(t<t1,SDL_Delay(t1-t))W(t1<t,eng();t1+=10;tmr+=tS)
         dr(cp[0]-RW/2,cp[1]-RH/2);rS(gS);SDL_Flip(g0))
 Y(mus,Mix_FreeMusic(mus))R 0;}
