#include"a.h" //MI:menu item,ML:label,MB:button,MO:options list,MK:key select
#define hh(x,a...) ST x:MI{x(Mn*,O C*);O C*text()O;I val()O,w(SF*);a;};
hh(MB,V ks())hh(ML,V dr(SF*,I,I,I,G))hh(MK,V ks(),upd(UC*);MK(Mn*,O C*,K);K _k;UC _ks[SDLK_LAST])
hh(MO,V kl(),kr();V aO(O C*,I);O C*txtO(O C*)O;ST{O C*s;I v;}_a[16];I _n,_i)
MI::MI(Mn*m,O C*s){_m=m;_s=s;}V MI::dr(SF*f,I x,I y,I w0,G g){SFw(g,f,x+(w0-w(f))/2,y,text());}
ML::ML(Mn*m,O C*s):MI(m,s){}I ML::val()O_(-1)I ML::w(SF*f)_(SFtw(f,_s))O C*ML::text()O_(_s)
V ML::dr(SF*f,I x,I y,I w0,G g){SFw(g,f,x+(w0-w(f))/2,y+10,_s);}
MB::MB(Mn*m,O C*s):MI(m,s){}O C*MB::text()O_(_s)I MB::val()O_(-1)I MB::w(SF*f)_(SFtw(f,text()))V MB::ks(){_m->sFi();}
MO::MO(Mn*m,O C*s):MI(m,s){_n=_i=0;}V MO::aO(O C*s,I v){assert(_n<ZZ(_a));_a[_n].s=s;_a[_n].v=v;_n++;}
I MO::val()O_(_n?_a[_i].v:-1)I MO::w(SF*f)_(SFtw(f,text()))O C*MO::text()O_(_n?txtO(_a[_i].s):_s)
V MO::kl(){_i=(_i+_n-1)%_n;}V MO::kr(){_i=(_i+1)%_n;}O C*MO::txtO(O C*o)O_(S C b[64];sprintf(b,"%s %s",_s,o);b)
MK::MK(Mn*m,O C*s,K k):MI(m,s){_k=k;}I MK::val()O_(-1)I MK::w(SF*f)_(SFtw(f,text()))
O C*MK::text()O_(S C b[64];sprintf(b,"%s %s",_s,SDL_GetKeyName(_k));b)V MK::ks(){i(SDLK_LAST,_ks[i]=0)_m->foc(this);}
V MK::upd(UC*k){i(SDLK_LAST,Y(_ks[i]&&!k[i],_m->foc(0);_k=(K)i)_ks[i]=k[i])}
//Mn:menu,MMn:main menu,VMn:video settings menu,KMn:keyboard configuration menu
Mn::Mn(){_show=_fin=_selMd=0;_r.x=g0->w/2;_r.y=g0->h/2;_r.w=_r.h=_n=_i=0;_foc=0;}S O I padW=15,padH=15;
Mn::~Mn(){}V Mn::add(MI*x){assert(_n<ZZ(_a));_a[_n++]=x;Y(_show,calcB())}V Mn::show(){calcB();_show=1;}
V Mn::upd(UC*k){S B u,d,l,r,s,e;P(_fin)P(_foc,_foc->upd(k);)
 B u1=k[SDLK_UP],d1=k[SDLK_DOWN],l1=k[SDLK_LEFT],r1=k[SDLK_RIGHT],s1=k[SDLK_RETURN],e1=k[SDLK_ESCAPE];
 Y(u1>u,u=1;MIX(s_beep);_i=(_i+_n-1)%_n)Y(l1>l,l=1;MIX(s_dec);_a[_i]->kl())Y(s1>s,s=1;MIX(s_sel);_a[_i]->ks())
 Y(d1>d,d=1;MIX(s_beep);_i=(_i+   1)%_n)Y(r1>r,r=1;MIX(s_inc);_a[_i]->kr())Y(e1>e,e=1;MIX(s_sel))
 u=u1;d=d1;l=l1;r=r1;s=s1;e=e1;}
V Mn::dr(){P(!_show)G g=g_bg;
 for(I y=0;y<_r.h;y+=g->h)for(I x=0;x<_r.w;x+=g->w){SR r,s;r.x=r.y=0;r.w=_r.w-x<g->w?_r.w-x:g->w;
  r.h=_r.h-y<g->h?_r.h-y:g->h;s.x=x+_r.x;s.y=y+_r.y;s.w=s.h=0;SB(g,&r,g0,&s);}
 I y=_r.y+padH;i(_n,MI*mi=_a[i];mi->dr(i==_i?fW:fY,_r.x,y,_r.w,g0);y+=30)}
V Mn::calcB(){_r.w=0;_r.h=_n*30+2*padH;_r.w=300+2*padW;_r.x=(g0->w-_r.w)/2;_r.y=(g0->h-_r.h)/2;}
V Mn::sFi(){_fin=1;_selMd=1;}V Mn::foc(MI*x){_foc=x;}
#define h(x) l->aO(#x " min",6000*x);
MMn::MMn(){add(new MB(this,"Play Game!"));MO*l=_hl=new MO(this,"Half Length:");add(l);h(1)h(2)h(5)h(10)h(20)h(45)l->_i=2;
 MO*p=_pls=new MO(this,"");p->aO("Player V Computer",1);p->aO("Player V Player",3);p->aO("Computer V Computer",0);add(p);
 i(3,add(new MB(this,(O C*[]){"Video Settings","Redefine Keys","Exit"}[i])))}
MMn::~MMn(){hl=_hl->val();I v=_pls->val();i(2,coAI[i]=!(v>>i&1))}
VMn::VMn(B b){add(_mo=new MO(this,"Video mode"));i(2,_mo->aO(i?"Fullscreen":"Windowed",i))_mo->_i=b;add(new MB(this,"Exit"));}
VMn::~VMn(){}B VMn::curMd()O_(_mo->val())KMn::~KMn(){i(2,j(6,co[i]._k[j]=_k[i][j]->_k))}
KMn::KMn(){S O C*a[]={"Player 1","Player 2"},*b[]={"Up:","Down:","Left:","Right:","Pass:","Shoot:"};
 i(2,add(new ML(this,a[i]));j(6,add(_k[i][j]=new MK(this,b[j],co[i]._k[j]))))add(new MB(this,"Exit"));}
