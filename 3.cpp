#include"a.h"
A::A(D x,D y,D z){_[0]=x;_[1]=y;_[2]=z;}
D&A::OPE[](I i)_(_[i])
A nrm(A a)_(D k=l1(a);k?mul(a,1/k):A{1})
D l2(A a)_(dot(a,a))
D l1(A a)_(sqrt(l2(a)))
B ll(A a,D d)_(l2(a)<d*d)
A add(A a,A b)_(i(3,a[i]+=b[i])a)
A sub(A a,A b)_(i(3,a[i]-=b[i])a)
A mul(A a,D k)_(i(3,a[i]*=k)a)
A z0(A a)_(A r=a;r[2]=0;r)
D dot(A a,A b)_(D r=0;i(3,r+=a[i]*b[i])r)
B inArc(A l,A a,A r)_(A v=nrm(a);dot(nrm(mmu(roll(M_PI_2),l)),v)>=0&&dot(nrm(mmu(roll(-M_PI_2),r)),v)>=0)
M::M(){i(4,j(4,_[i][j]=i==j))}
A mmu(M m,A v)_(A r;i(3,r._[i]=dot(v,(A){m._[0][i],m._[1][i],m._[2][i]}))r)
M arb(A d,D a)_(M m;O D s=sin(a),c=cos(a);i(3,j(3,m._[i][j]=d._[i]*d._[j]*(1-c)+d._[i^j^3]*s*(1-(i-j+4)%3)+(i==j)*c))m)
M roll(D a)_(M r;r._[0][0]=r._[1][1]=cos(a);r._[1][0]=-(r._[0][1]=sin(a));r)
