#include"a.h"
#define h M_SQRT1_2
A DIR[]={{0,1},{h,h},{1,0},{h,-h},{0,-1},{-h,-h},{-1,0},{-h,h}};
S O D mvSlwdn=.7,wD2=5e3,rD2=2e4,lV=1.5,wV=2.6,rV=3.5,mvV[]={0,lV,wV,rV,rV*2,rV*3,rV,rV,rV*2,rV}; //w:walk,r:run,l:limp
V iPl(Pl*x,B t,B g){x->_t=t;x->_gk=g;x->_cmitT=0;x->_kkCntdn=0;x->_tcTmr=0;x->_jt=0;x->_drblTmr=0;x->_dir=0;x->_dirCnt=0;x->_m=Sta;
 i(5,iSq(x->_sq+i,x->_t,x->_gk,i))iQ(&x->_q,0,0,0,x->_p,SR{-32,-54,0,0},SR{-14,-60,0,0},SR{-16,-60,0,0});aQ(&x->_q);}
S I kkPrio(Pl*x)_(x->_gk&&!x->_tcTmr?5:2-(x->_tcTmr>0)+(x->_m==Tkl)+2*(x->_m==Hvy)-(x->_m==Sta))
S Sq*sq(Pl*x)_(&x->_sq["100233224"[x->_m]&7])
S V mvPl(Pl*x){x->_v[2]-=grv;x->_p=add(x->_p,x->_v);Y(x->_p[2]<0,x->_p[2]=0)
 Y(x->_m==Tkl||x->_m==Hvy||(x->_m==Hdr&&!x->_p[2]),x->_v[0]*=fri;x->_v[1]*=fri)
 x->_q._g[2]=!x->_ac?0:x->_t?g_m1:g_m0;x->_q._p=x->_p;Sq*s=sq(x);x->_q._g[0]=s->_g[x->_dir][s->_i];}
#define DEC(x) x-=x>0;
V uPl(Pl*x){DEC(x->_tcTmr)DEC(x->_cmitT)DEC(x->_kkCntdn)DEC(x->_drblTmr)
 Y(x->_cS==Full,A gv={x->_v[0],x->_v[1]}; //try trap ball
  Y(l2(gv)>0&&fabs(bp[2]-x->_p[2])<kkH&&tcngBa(x,0)&&x->_m-Sht&&x->_m-Pas,A v; //required velocity
   Y(x->_m==Lmp||x->_m==Wlk||x->_m==Run,
    v=mul(add(add(x->_p,mul(x->_v,10)),sub(mul(nrm(gv),32),bp)),.07);v[2]=bp[2]?bv[2]:.1+grv)
   E(v=mul(x->_v,1.3))
   Y(l1(sub(v,bv))/l1(bv)>.05,kkBa(v,kkPrio(x),x);x->_tcTmr=100))
  Y(!x->_ac,
   Y(x->_gk,A w=x->_d;
    Y(inA(bp,x->_d[1]<OY),w=bp)
    E(A v=sub(bp,x->_d);D k=l1(v);Y(k,v=mul(v,250/k);k=v[1];Y(k<0,k=-k)Y(k>10,v=A(v[0],v[1]/10,v[2]))w=add(x->_d,v)))
    D d2=l2(sub(x->_p,w     ));Y(d2<lV*lV,sMv(x,Sta);sDir(x,clDir(x,bp)))E(Y(x->_m-Run&&x->_m-Wlk,sMv(x,Run))Y(d2<wD2,sMv(x,Wlk))EY(d2>rD2,sMv(x,Run))sDir(x,clDir(x,w))))
   E(D d2=l2(sub(x->_p,x->_d));Y(d2<lV*lV,sMv(x,Sta);sDir(x,clDir(x,bp)))E(Y(x->_m-Run&&x->_m-Wlk,sMv(x,Run))Y(d2<wD2,sMv(x,Wlk))EY(d2>rD2,sMv(x,Run))sDir(x,clDir(x,x->_d))))))
 EY(x->_cS==K1,sMv(x,Sta);Y(!x->_ac,sDir(x,clDir(x,bp))))
 E(D d2=l2(sub(x->_p,x->_d));
  Y(d2<lV*lV,sMv(x,Sta);sDir(x,clDir(x,bp)))
  E(I d=x->_dir;Y(++x->_dirCnt>100||l2(add(x->_p,sub(mul(DIR[x->_dir],lV),x->_d)))>=d2,d=clDir(x,x->_d);x->_dirCnt=0)
   sMv(x,d2<wV*wV?Lmp:d2<rV*rV?Wlk:Run);sDir(x,d)))
 x->_jt=0;mvPl(x);uQ(sq(x));}
B sMv(Pl*x,Mv m,I var){A v;P(cmtd(x)||m==x->_m,0)
 SW(m){CA Sta:CA Lmp:CA Wlk:CA Run:BR;CA Tkl:x->_cmitT=140;BR;CA Hvy:x->_cmitT=200;BR;CA Hdr:x->_cmitT=200;BR;CA Fal:BR;
  CA Pas:{P(x->_cS==Nn||!x->_ac||!tcngBa(x,1),0)Pl*pl=plFmP(tm+x->_t,x->_p,x->_dir);
   Y(!pl,v=mul(DIR[x->_dir],rV*1.4);v[2]=2)
   E(v=calcReqV(2,pl->_p);D mn=rV*1.2,mx=rV*3,d2=l2(v);
    Y(d2<mn*mn,v[2]=0;v=mul(nrm(v),mn);v[2]=2)Y(d2>mx*mx,v[2]=0;v=mul(nrm(v),mx);v[2]=2))
   kkBa(v,kkPrio(x)+1,x);x->_cmitT=50;BR;}
  CA Sht:{P(x->_cS==Nn||!x->_ac||!tcngBa(x,1),0)
   v=mul(DIR[x->_dir],var*rV*1.5/100);v[2]=3;kkBa(v,kkPrio(x)+1,x);x->_cmitT=50;x->_kkCntdn=50;BR;}}
 D k=mvV[m];Y(x->_tcTmr,k*=mvSlwdn)x->_v=mul(DIR[x->_dir],k);Y(m==Hdr,x->_v[2]+=2.5)x->_m=m;sq(x)->_i=0;R 1;}
B inP(Pl*x)_(ll(sub(x->_p,x->_d),lV))
V sDir(Pl*x,I d){P(d<0||d>7||cmtd(x))x->_jt|=d!=x->_dir;x->_dir=d;D k=mvV[x->_m];Y(x->_tcTmr,k*=mvSlwdn)
 A v=mul(DIR[x->_dir],k);
 Y(x->_cS==Full&&x->_ac&&l2(add(x->_p,sub(v,bp)))<l2(sub(x->_p,bp)), //"guidance"
  D a=M_PI/18;A l=mmu(roll(-a),DIR[x->_dir]),r=mmu(roll(a),DIR[x->_dir]);
  Y(inArc(l,sub(bp,x->_p),r),v=mul(nrm(z0(sub(bp,x->_p))),k)))
 x->_v=v;}
V sPlCS(Pl*x,CS y){x->_cS=y;x->_ac&=y!=Nn;}
I clDir(Pl*x,O A&p)_(I d=0;D l=l2(sub(x->_p,p));x->_dirCnt=0;
 i(8,A e=add(x->_p,sub(mul(DIR[i],.01),p));Y(l2(e)<l,d=i;l=l2(e)))d)
B cmtd(Pl*x)_(x->_cmitT>0)
B aft(Pl*x,I d)_(P(!x->_kkCntdn,0)A u=bv;u[2]=0;P(ll(u,1e-4),x->_kkCntdn=0;0)
 u=mul(u,1/l1(u));u=mmu(roll(M_PI_2),u);bv=add(bv,mul(mmu(arb(u,1.2),DIR[d]),.07));1)
B tcngBa(Pl*x,B sht)_(P(x->_cS-Full&&x->_cS-K1,0)D d=l2(sub(bp,x->_p));bp[2]<kkH&&((x->_jt&&d<600)||d<350||(sht&&d<1e3)))
