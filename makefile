C=g++ -Wall --max-errors=1
f:$(patsubst %.cpp,o/%.o,$(wildcard *.cpp)) $(patsubst %.c,o/%.o,$(wildcard *.c))
	$(C) $^ -o $@ -lm -lSDL -lSDL_image -lSDL_mixer
o/%.o:%.cpp *.h makefile
	@mkdir -p o
	$(C) -c $< -o $@
o/%.o:%.c *.h makefile
	@mkdir -p o
	$(C) -c $< -o $@
